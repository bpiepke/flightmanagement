﻿using FlightManagement.AirportDatalayer;
using FlightManagement.AirportDatalayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportBuisnessLayer.Services
{
    public interface IAirlineService
    {
        int AddAirline(Airline airline);
        Airline GetAirlineById(int airlineId);
        bool AirlineVerivy(int airlineId);
        List<Airline> GetAllAirLines();
    }
    public class AirlineService : IAirlineService

    {
        private readonly Func<IFlightManagementDbContext> _dbContextFactory;

        public AirlineService(Func<IFlightManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public int AddAirline(Airline airline)
        {
            int Id;
            using (var context = new FlightManagementDbContext())
            {
                var newAirlineEntry = context.Airlines.Add(airline);
                var addedAirlineEntry = context.Airlines.Add(airline);
                context.SaveChanges();

                Id = addedAirlineEntry.Entity.Id;
            }
            return Id;
        }

        public Airline GetAirlineById(int airlineId)
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Airlines.FirstOrDefault(l => l.Id == airlineId);
            }
        }

        public bool AirlineVerivy(int airlineId)
        {
            Airline inputID = GetAirlineById(airlineId);
            if (inputID == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Airline> GetAllAirLines()
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Airlines
                    .ToList();
            }
        }
    }
}
