﻿using FlightManagement.AirportDatalayer;
using FlightManagement.AirportDatalayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportBuisnessLayer.Services
{
    public interface IFlightService
    {
        int AddFlight(Flight flight);
        Flight GetFlightById(int flightId);
        bool FlightVerivy(int flightId);
        List<Flight> GetAllFlights();
    }
    public class FlightService : IFlightService
    {
        private readonly Func<IFlightManagementDbContext> _dbContextFactory;

        public FlightService(Func<IFlightManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public int AddFlight(Flight flight)
        {
            int Id;
            using (var context = new FlightManagementDbContext())
            {
                var newFlightEntry = context.Flights.Add(flight);
                var addedFlightEntry = context.Flights.Add(flight);
                context.SaveChanges();

                Id = addedFlightEntry.Entity.Id;
            }
            return Id;
        }

        public Flight GetFlightById(int flightId)
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Flights.FirstOrDefault(l => l.Id == flightId);
            }
        }

        public bool FlightVerivy(int flightId)
        {
            Flight inputID = GetFlightById(flightId);
            if (inputID == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List <Flight> GetAllFlights()
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Flights
                    .Include(l => l.Airport)
                    .Include(l => l.Plane)
                    .Include(l => l.Airline)
                    .ToList();
            }
        }

    }
}
