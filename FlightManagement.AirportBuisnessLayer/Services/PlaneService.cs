﻿using FlightManagement.AirportDatalayer;
using FlightManagement.AirportDatalayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportBuisnessLayer.Services
{
    public interface IPlaneService
    {
        Plane AddPlane(Plane plane);
        Plane GetPlaneById(int planeId);
        bool PlaneVerivy(int planeId);
        List<Plane> GetAllPlanes();

    }
    public class PlaneService : IPlaneService
    {
        public Plane AddPlane(Plane plane)
        {
            using (var context = new FlightManagementDbContext())
            {
                if (context.Planes.FirstOrDefault(l => l.Model == plane.Model) != null)
                {
                    throw new Exception($"The author {plane.Model} already exists.");
                }

                var addEntity = context.Planes.Add(plane);

                context.SaveChanges();

                return addEntity.Entity;
            }
        }

        public Plane GetPlaneById(int planeId)
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Planes.FirstOrDefault(l => l.Id == planeId);
            }
        }
        public bool PlaneVerivy(int planeId)
        {

            Plane inputID = GetPlaneById(planeId);
            if (inputID == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Plane> GetAllPlanes()
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Planes
                    .ToList();
            }
        }
    }
}
