﻿using FlightManagement.AirportDatalayer;
using FlightManagement.AirportDatalayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportBuisnessLayer.Services
{
    public interface IAirtportService
    {
        Airport AddAirport(Airport airport);
        Airport GetAirportById(int airportId);
        bool AirportVerivy(int airportId);
        List<Airport> GetAllAirports();
    }

    public class AirtportService : IAirtportService
    {
        private readonly Func<IFlightManagementDbContext> _dbContextFactory;

        public AirtportService(Func<IFlightManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public Airport AddAirport(Airport airport)
        {
            using (var context = new FlightManagementDbContext())
            {
                if (context.Airtports.FirstOrDefault(l => l.AirPortName == airport.AirPortName) != null)
                {
                    throw new Exception($"The author {airport.AirPortName} already exists.");
                }

                var addEntity = context.Airtports.Add(airport);

                context.SaveChanges();

                return addEntity.Entity;
            }
        }

        public Airport GetAirportById(int airportId)
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Airtports.FirstOrDefault(l => l.Id == airportId);
            }
        }

        public bool AirportVerivy(int airportId)
        {
            Airport inputID = GetAirportById(airportId);
            if (inputID == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List <Airport> GetAllAirports()
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Airtports
                    .ToList();
            }
        }
    }
}
