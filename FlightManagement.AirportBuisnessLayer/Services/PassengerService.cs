﻿using FlightManagement.AirportDatalayer;
using FlightManagement.AirportDatalayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightManagement.AirportBuisnessLayer.Services
{
    public interface IPassengerService
    {
        int AddPassenger(Passenger passenger);
        List<Passenger> GetAllPassenger();
    }
    public class PassengerService : IPassengerService
    {
        private readonly Func<IFlightManagementDbContext> _dbContextFactory;

        public PassengerService(Func<IFlightManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public int AddPassenger(Passenger passenger)
        {
            int Id;
            using (var context = new FlightManagementDbContext())
            {
                var newPassengerEntry = context.Passengers.Add(passenger);
                var addedPassengerEntry = context.Passengers.Add(passenger);
                context.SaveChanges();

                Id = addedPassengerEntry.Entity.Id;
            }
            return Id;
        }

        public List<Passenger> GetAllPassenger()
        {
            using (var context = new FlightManagementDbContext())
            {
                return context.Passengers
                    .ToList();
            }
        }
    }
}
