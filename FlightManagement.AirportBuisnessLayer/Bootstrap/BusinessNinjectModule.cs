﻿using FlightManagement.AirportBuisnessLayer.Services;
using Ninject.Modules;

namespace FlightManagement.AirportBuisnessLayer.Bootstrap
{
    public class BusinessNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IAirlineService>().To<AirlineService>();
            Kernel.Bind<IAirtportService>().To<AirtportService>();
            Kernel.Bind<IFlightService>().To<FlightService>();
            Kernel.Bind<IPassengerService>().To<PassengerService>();
            Kernel.Bind<IPlaneService>().To<PlaneService>();


            Kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>();
        }
    }
}

