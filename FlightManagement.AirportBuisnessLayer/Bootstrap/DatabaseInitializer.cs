﻿using FlightManagement.AirportDatalayer;
using System;

namespace FlightManagement.AirportBuisnessLayer.Bootstrap
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly Func<IFlightManagementDbContext> _dbContextFactory;

        public DatabaseInitializer(Func<IFlightManagementDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public void Initialize()
        {
            using (var ctx = new FlightManagementDbContext())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}
