﻿using FlightManagement.AirportDatalayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;

namespace FlightManagement.AirportDatalayer
{
    public interface IFlightManagementDbContext : IDisposable
    {
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Airport> Airtports { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<Plane> Planes { get; set; }
        DatabaseFacade Database { get; }
        int SaveChanges();
    }
    public class FlightManagementDbContext : DbContext, IFlightManagementDbContext
    {
        private const string _connectionString = "Data Source=. ;Initial Catalog=FlightManagementDb;Integrated Security=True;";

        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Airport> Airtports { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<Plane> Planes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

}
