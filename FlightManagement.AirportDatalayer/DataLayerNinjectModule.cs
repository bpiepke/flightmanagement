﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer
{
    public class DataLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IFlightManagementDbContext>().ToMethod(x => new FlightManagementDbContext());
        }
    }
}