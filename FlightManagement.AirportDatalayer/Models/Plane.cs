﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer.Models
{
    public class Plane
    {
        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int NumberOfSeats { get; set; }
    }
}
