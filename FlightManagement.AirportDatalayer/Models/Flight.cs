﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public int PlaneId { get; set; }
        public Plane Plane { get; set; }
        public int AirlineId { get; set; }
        public Airline Airline { get; set; }
        public int AirportId { get; set; }
        public Airport Airport { get; set; }
        public int FlightCode { get; set; }
        public double Price { get; set; }
        public DateTime HourOfFlight { get; set; }
        public int DayOfWeek { get; set; }

    }
}
