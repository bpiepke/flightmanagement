﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer.Models
{
    public class Airline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AirLineCode { get; set; }
    }
}
