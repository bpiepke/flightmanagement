﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer.Models
{
    public class Airport
    {
        public int Id { get; set; }
        public string AirPortName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Coordinates { get; set; }
    }
}
