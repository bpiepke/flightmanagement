﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightManagement.AirportDatalayer.Models
{
    public class Passenger
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime YearOfBorn { get; set; }
    }
}
