﻿using FlightManagement.AirportBuisnessLayer.Bootstrap;
using FlightManagement.AirportDatalayer;
using Ninject;
using Ninject.Modules;

namespace System_FlightManagement.Bootstrap
{
    public class DependencyResolver
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new INinjectModule[]
            {
              new CliNinjectModule(),
              new BusinessNinjectModule(),
              new DataLayerNinjectModule()
            });

            return kernel;
        }
    }
}
