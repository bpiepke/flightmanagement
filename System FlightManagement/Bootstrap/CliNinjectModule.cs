﻿using FlightManagement.AirportClient;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System_FlightManagement.AirportClient.Helpers;

namespace System_FlightManagement.Bootstrap
{
    public class CliNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IProgram>().To<Program>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
        }
    }
}
