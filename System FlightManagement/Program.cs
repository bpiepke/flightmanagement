﻿using FlightManagement.AirportBuisnessLayer;
using FlightManagement.AirportBuisnessLayer.Bootstrap;
using FlightManagement.AirportBuisnessLayer.Services;
using FlightManagement.AirportDatalayer.Models;
using Ninject;
using System;
using System_FlightManagement;
using System_FlightManagement.AirportClient.Helpers;
using System_FlightManagement.Bootstrap;

namespace FlightManagement.AirportClient
{

    public interface IProgram
    {
        void Run();
    }


    internal class Program : IProgram
    {
        private readonly IIoHelper _ioHelper;
        private readonly IDatabaseInitializer _databaseInitializer;
        private readonly IAirtportService _airportService;
        private readonly IPlaneService _planeService;
        private readonly IAirlineService _airlineService;
        private readonly IFlightService _flightService;
        private readonly IPassengerService _passengerService;
        static void Main(string[] args)
        {
            using (var kernel = DependencyResolver.GetKernel())
            {
                var program = kernel.Get<IProgram>();
                program.Run();
            }
        }

        public Program(
            IIoHelper ioHelper,
            IDatabaseInitializer databaseInitializer,
            IAirtportService airportService,
            IPlaneService planeService,
            IAirlineService airlineService,
            IFlightService flightService,
            IPassengerService passengerService)
        {
            _ioHelper = ioHelper;
            _databaseInitializer = databaseInitializer;
            _airportService = airportService;
            _planeService = planeService;
            _airlineService = airlineService;
            _flightService = flightService;
            _passengerService = passengerService;

        }

        public void Run()
        {
            InitializeDatabase();
            DisplayMenu();
        }

        private void InitializeDatabase()
        {
            _databaseInitializer.Initialize();

        }

        private void DisplayMenu()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("Choose the option below.\n");
                Console.WriteLine("1. Add new airport");
                Console.WriteLine("2. Add new plane");
                Console.WriteLine("3. Add new airlines");
                Console.WriteLine("4. Add new flight");
                Console.WriteLine("5. Add new passenger");
                Console.WriteLine("6. Show all airports");
                Console.WriteLine("7. Show all planes");
                Console.WriteLine("8. Show all airlines");
                Console.WriteLine("9. Show all flights");
                Console.WriteLine("10. Show all passenger");
                Console.WriteLine("11. Exit \n");

                var input = _ioHelper.GetIntFromUser("Enter the number");

                Console.Clear();

                switch (input)
                {
                    case 1:
                        AddAirport();
                        break;
                    case 2:
                        AddPlane();
                        break;
                    case 3:
                        AddAirlines();
                        break;
                    case 4:
                        AddFlight();
                        break;
                    case 5:
                        AddPassenger();
                        break;
                    case 6:
                        ShowAllAirports();
                        break;
                    case 7:
                        ShowAllPlanes();
                        break;
                    case 8:
                        ShowAllAirlines();
                        break;
                    case 9:
                        ShowFlights();
                        break;
                    case 10:
                        ShowAllPassenger();
                        break;
                    case 11:
                        return;
                    default:
                        Console.WriteLine($"Number {input} is invalid. Please try again.");
                        _ioHelper.PressKeyToContinue();
                        break;
                };
            }
        }

        private void AddAirport()
        {
            Console.WriteLine("You want to add a new Airport. Please enter the data below.\n");
            Airport newAirport = new Airport()
            {
                AirPortName = _ioHelper.GetStringFromUser("Airport name"),
                City = _ioHelper.GetStringFromUser("City"),
                Country = _ioHelper.GetStringFromUser("Country"),
                Coordinates = _ioHelper.GetCoordinateFromUser("Write coordinates in format: xx.xxxxxx, xx.xxxxxx"),
            };
            _airportService.AddAirport(newAirport);

            Console.WriteLine("Airport Added");

            _ioHelper.PressKeyToContinue();
        }

        private void AddPlane()
        {
            Console.WriteLine("You want to add a new Plane. Please enter the data below.\n");
            Plane newPlane = new Plane()
            {
                Manufacturer = _ioHelper.GetStringFromUser("Manufacturer"),
                Model = _ioHelper.GetStringFromUser("Model"),
                NumberOfSeats = _ioHelper.GetIntFromUser("Number of Seats"),
            };
            _planeService.AddPlane(newPlane);

            Console.WriteLine("Plane Added");

            _ioHelper.PressKeyToContinue();
        }

        private void AddAirlines()
        {
            Console.WriteLine("You want to add a new Airlines. Please enter the data below.\n");
            Airline newAirline = new Airline()
            {
                Name = _ioHelper.GetStringFromUser("Name"),
                AirLineCode = _ioHelper.GetIntFromUser("Airline code")
            };
            _airlineService.AddAirline(newAirline);

            Console.WriteLine("Airline Added");

            _ioHelper.PressKeyToContinue();
        }

        private void AddFlight()
        {
            var planeId = _ioHelper.GetIntFromUser("Enter the ID of Plane");

            var planeverivy = _planeService.PlaneVerivy(planeId);
            if (planeverivy == false)
            {
                Console.WriteLine("Flight with this ID is not exsist.");
                _ioHelper.PressKeyToContinue();
                return;
            }
            _ioHelper.PressKeyToContinue();
            Console.Clear();


            var airlineId = _ioHelper.GetIntFromUser("Enter the AirLine ID");

            var airlineverivy = _airlineService.AirlineVerivy(airlineId);
            if (airlineverivy == false)
            {
                Console.WriteLine("Airline with this ID is not exsist.");
                _ioHelper.PressKeyToContinue();
                return;
            }
            _ioHelper.PressKeyToContinue();
            Console.Clear();

            var airportId = _ioHelper.GetIntFromUser("Enter the Airport ID");
            var airportverivy = _airportService.AirportVerivy(airportId);
            if (airportverivy == false)
            {
                Console.WriteLine("Airline with this ID is not exsist.");
                _ioHelper.PressKeyToContinue();
                return;
            }
            _ioHelper.PressKeyToContinue();
            Console.Clear();


            Flight newFlight = new Flight()
            {
                FlightCode = _ioHelper.GetNumberOfFlight("Enter the flight Code (5 character)"),
                Price = _ioHelper.GetDoubleFromUser("Enter the price"),
                PlaneId = planeId,
                AirlineId = airlineId,
                AirportId = airportId,
                HourOfFlight = _ioHelper.GetHour("Enter the date of Flight"),
                DayOfWeek = _ioHelper.GetDayFromUser("Enter the day of week")
            };
            _flightService.AddFlight(newFlight);

            Console.WriteLine("Flight Added");

            _ioHelper.PressKeyToContinue();
        }

        private void AddPassenger()
        {
            Passenger newPassenger = new Passenger()
            {
                FirstName = _ioHelper.GetStringFromUser("Enter the first name"),
                LastName = _ioHelper.GetStringFromUser("Enter the last name"),
                YearOfBorn = _ioHelper.GetDateFromUser("Enter the date of born")

            };
            _passengerService.AddPassenger(newPassenger);

            Console.WriteLine("Passenger Added");

            _ioHelper.PressKeyToContinue();
        }

        private void ShowAllAirports()
        {
            foreach (var airport in _airportService.GetAllAirports())
            {
                Console.WriteLine($"ID: {airport.Id} Airport name: {airport.AirPortName} City: {airport.City} Country: {airport.Country} Coordinates: {airport.Coordinates}");
            }
            _ioHelper.PressKeyToContinue();
        }

        private void ShowAllPlanes()
        {
            foreach (var plane in _planeService.GetAllPlanes())
            {
                Console.WriteLine($"ID: {plane.Id} Manufacturer: {plane.Manufacturer} Model: {plane.Model} Number of seats: {plane.NumberOfSeats}");
            }
            _ioHelper.PressKeyToContinue();
        }

        private void ShowAllAirlines()
        {
            foreach (var airline in _airlineService.GetAllAirLines())
            {
                Console.WriteLine($"ID: {airline.Id} Name: {airline.Name} AirLine Code: {airline.AirLineCode}");
            }
            _ioHelper.PressKeyToContinue();
        }

        private void ShowFlights()
        {
            foreach (var flight in _flightService.GetAllFlights())
            {
                Console.WriteLine($"ID: {flight.Id} Plane: {flight.Plane.Model}  Destination airports: {flight.Airline.Name} Destination: {flight.Airport.AirPortName} Flight Code: {flight.Airline.AirLineCode}-{flight.FlightCode} Hour: {flight.HourOfFlight.ToString("HH:mm")} Day of Week: {flight.DayOfWeek} Price:"+  String.Format("{0:N2}", flight.Price));
            }
            _ioHelper.PressKeyToContinue();
        }


        private void ShowAllPassenger()
        {
            foreach (var passenger in _passengerService.GetAllPassenger())
            {
                Console.WriteLine($"ID {passenger.Id} Firstname: {passenger.FirstName} Lastname: {passenger.LastName}");
            }
        }
    }
}
