﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace System_FlightManagement.AirportClient.Helpers
{
    internal interface IIoHelper
    {
        DateTime GetDateFromUser(string message);
        DateTime GetHour(string message);
        int GetDayFromUser(string message);
        int GetIntFromUser(string message);
        string GetStringFromUser(string message);
        void PressKeyToContinue();
        string GetCoordinateFromUser(string message);
        int GetNumberOfFlight(string message);
        double GetDoubleFromUser(string message);

    }
    internal class IoHelper : IIoHelper
    {
        public string GetCoordinateFromUser(string message)
        {
            var input = "";
            var success = false;
            string rgx0 = @"\d\d\d.\d\d\d\d\d\d, \d\d.\d\d\d\d\d\d";
            string rgx1 = @"\d\d.\d\d\d\d\d\d, \d\d\d.\d\d\d\d\d\d";
            string rgx2 = @"\d\d\d.\d\d\d\d\d\d, \d\d\d.\d\d\d\d\d\d";
            string rgx3 = @"\d\d.\d\d\d\d\d\d, \d\d.\d\d\d\d\d\d";


            while (!success)
            {
                Console.Write($"{message}: ");
                input = Console.ReadLine();
                success = (Regex.IsMatch(input, rgx0) || Regex.IsMatch(input, rgx1) || Regex.IsMatch(input, rgx2) || Regex.IsMatch(input, rgx3));

                if (!success)
                {
                    Console.WriteLine($"'{input}' is not a Coordinates. Please try again.");
                }
            }
            return input;

        }
        public DateTime GetDateFromUser(string message)
        {
            const string dateFormat = "dd-MM-yyyy";
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }

            return result;
        }

        public DateTime GetHour(string message)
        {
            const string dateFormat = "HH:mm";
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }
            return result;
        }

        public int GetDayFromUser(string message)
        {
            var intinput = 11;
            Console.Write($"{message}: ");
            var input = Console.ReadLine();
            int.TryParse(input, out intinput);

            while (intinput > 7)
            {
                Console.WriteLine($"'{input}' is not a day of week. Please try again.");
                Console.Write($"{message}: ");
                input = Console.ReadLine();
                int.TryParse(input, out intinput);
            }
            return intinput;
        }

        public int GetIntFromUser(string message)
        {
            var result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine($"'{input}' is not a number. Please try again.");
                }
            }
            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            return Console.ReadLine();
        }

        public void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public int GetNumberOfFlight(string message)
        {
            var result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                int lenth = input.Length;
                if (lenth == 5)
                {
                    success = int.TryParse(input, out result);

                    if (!success)
                    {
                        Console.WriteLine($"'{input}' is not a flight number. Please try again.");
                    }
                }
                else
                {
                    Console.WriteLine($"'{input}' is not a flight number. Please try again.");
                };
            }
            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number");
                }
            }

            return result;
        }

    }
}
